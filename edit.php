<?php

include "connection.php";

$data=$db->query("select * from data where id=".$_GET["id"]);

$data_barang=$data->fetchAll();
?>

<!doctype html>
<html lang="en">
<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>Form Edit</title>
</head>
<body class="bg-secondary">
<!-- Action edit -->
<div class="container">
    <div class="row vh-100 justify-content-center">
        <div class="col-6 border rounded border-dark p-4 align-self-center">
            <h1 class="text-light">Apa Yang Ingin Anda Edit ?</h1>
            <hr style="background-color: black;">
            <form action="update.php" method="POST">
                <input type="hidden" name="id" value="<?php echo $data_barang[0]["id"]; ?>">
            <div class="form-group">
                <label class="text-light" for="exampleInputEmail1">Nama Barang</label>
                <input type="text" autocomplete="off" name="nama_barang" value="<?php echo $data_barang[0]["nama_barang"]; ?>" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label class="text-light" for="exampleInputPassword1">Kode Barang</label>
                <input type="text" autocomplete="off" name="kode_barang" value="<?php echo $data_barang[0]["kode_barang"]; ?>" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="form-group">
                <label class="text-light" for="exampleInputPassword1">Harga</label>
                <input type="number" autocomplete="off" name="harga" value="<?php echo $data_barang[0]["harga"]; ?>" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="form-group">
                <label class="text-light" for="exampleInputPassword1">Stok</label>
                <input type="number" autocomplete="off" name="stok" value="<?php echo $data_barang[0]["stok"]; ?>" class="form-control" id="exampleInputPassword1">
            </div>
            <a href="data.php" type="button" class="btn btn-dark">Batal</a>
            <button type="submit" class="btn btn-primary mx-3">Edit</button>
            </form>
        </div>
    </div>
</div>

  <!-- Optional JavaScript; choose one of the two! -->
  <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</body>
</html>