<?php 

include "connection.php";
include "cari.php";

$nama=$db->query("select * from data");
$data_barang=$nama->fetchAll();

foreach ($data_barang as $key) {

}

// search pencarian
$penampung=[];
if(isset($_POST['cari']))
{
  $penampung=searching($_POST['search']);
}
if (!empty($penampung))
{
  $data_barang=$penampung;
}

// Search Action
if(isset($_POST['search']))
{

  $filter=$db->quote($_POST['search']);  

  $name=$_POST['search'];

  $search=$db->prepare("select * from data where nama_barang=? or kode_barang=? or harga=? or stok=?");

  $search->bindValue(1,$name,PDO::PARAM_STR);
  $search->bindValue(2,$name,pdo::PARAM_STR);
  $search->bindValue(3,$name,pdo::PARAM_INT);
  $search->bindValue(4,$name,pdo::PARAM_INT);

  $search->execute();

  $tampil_data=$search->fetchAll(); 

  $row = $search->rowCount();
}else{
  $data = $db->query("select * from data");

  $tampil_data = $data->fetchAll();
}
?>


<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/solid.css" integrity="sha384-yo370P8tRI3EbMVcDU+ziwsS/s62yNv3tgdMqDSsRSILohhnOrDNl142Df8wuHA+" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/brands.css" integrity="sha384-/feuykTegPRR7MxelAQ+2VUMibQwKyO6okSsWiblZAJhUSTF9QAVR0QLk6YwNURa" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/fontawesome.css" integrity="sha384-ijEtygNrZDKunAWYDdV3wAZWvTHSrGhdUfImfngIba35nhQ03lSNgfTJAKaGFjk2" crossorigin="anonymous">
<link rel="stylesheet" href="css/styles.css">
<link rel="shortcut icon" href="image/icon.jpg" type="image/x-icon">
<title>Street Clothing</title>
</head>
<body>

<!-- Navbar -->
<nav id="coba" class="navbar navbar-light bg-dark" style="z-index: 1;">
  <img class="pt-1" src="image/logo.png" width="110" height="40">
  <h3 class="text-light pt-1">Street Clothing Official Store  <i class="fas fa-shopping-cart"></i></h3>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Tambahkan Data Baru  <i class="fas fa-plus-square"></i>
  </button>
</nav>

<!-- Sidebar -->
<div class="sidebar col-12 col-sm-12 col-md-12 col-lg-12 text-center">
  <img class="mt-4" src="image/2.png" width="150" height="100">
  <h3 style="font-family: Calibry" class="mt-1">Street Clothing</h3>
  <hr class="bg-dark mt-5">
  <h3 class="mt-3"><a class="btn-outline-secondary" href="index.php"><i class="fas fa-home"></i> Home</a></h3><hr class="bg-dark">
  <h4 class="mt-3"><a class="btn-outline-secondary" href="dashboard.php"><i class="fas fa-columns"></i> Dashboard</a></h4><hr class="bg-dark">
  <p id="seting" class="text-center">Project Assessement<br>&copy;Munif Soleh | 2020</p>
</div>

<!-- Content -->
<div class="content">
  <div class="row" style="z-index: -1;">
    <div class="col col-12 col-sm-12 col-md-12 col-lg-12">
      <!----- Allert Massage ----->
      <?php if(isset($row)):?>
      <div class="alert alert-primary alert-dismissible fade show" role="alert">
        <p class="lead "><?php echo $row;?> Data Ditemukan !</p>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
      <?php endif; ?>
      <form class="form-inline my-2 my-lg-0" action="data.php" method="POST">
        <input name="search" class="form-control mr-sm-2" type="text" name="search" placeholder="Cari Data" autocomplete="off" aria-label="Search">
        <button name="cari" class="btn btn-primary my-2 my-sm-0" type="submit">Cari</button>
      </form>
      <!-- Table content -->
      <table class="table table-striped">
      <thead class="bg-primary">
        <tr>
          <th scope="col">No</th>
          <th scope="col">Nama Barang</th>
          <th scope="col">Kode Barang</th>
          <th scope="col">Harga</th>
          <th scope="col">Stok</th>
          <th scope="col">Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php $n=1 ; ?>
        <?php foreach ($data_barang as $key) : ?>
        <tr>
          <td><?php echo $n; ?></td>
          <td><?php echo $key["nama_barang"]; ?></td>
          <td><?php echo $key["kode_barang"]; ?></td>
          <td><?php echo $key["harga"]; ?></td>
          <td><?php echo $key["stok"]; ?></td>
          <td><a class="btn btn-danger" href="delete.php?id=<?php echo $key['id']; ?>">hapus</a> |
          <a class="btn btn-warning" href="edit.php?id=<?php echo $key["id"]; ?>">edit</a></td>
        </tr>
        <?php $n++ ; ?>
        <?php endforeach; ?>
      </tbody>
      </table>
    </div>
  </div>
</div>

<!-- Modal From Input Data -->
<div class="modal fade col-9 col-sm-9 col-md-12" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Masukan Data</h5>
      </div>
      <div class="modal-body">
        <form action="input.php" method="POST">
          <div class="form-group">
            <label for="exampleInputEmail1">Nama Barang</label>
            <input type="text" autocomplete="off" name="nama_barang" required class="form-control">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Kode Barang</label>
            <input type="text" autocomplete="off" name="kode_barang" required class="form-control">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Harga</label>
            <input type="number" autocomplete="off" name="harga" required class="form-control">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Stok</label>
            <input type="number" autocomplete="off" name="stok" required class="form-control">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button> |
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</body>
</html>