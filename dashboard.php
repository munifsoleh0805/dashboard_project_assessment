<?php 

include "connection.php";
include "cari.php";

$nama=$db->query("select * from data");
$data_barang=$nama->fetchAll();
// var_dump($data_barang);die;

$temp_hrg = [];
foreach ($data_barang as $value) {
    $temp_hrg[]=$value['harga'];

    $max = max($temp_hrg);
    $min = min($temp_hrg);
}
// var_dump($min); exit;

$temp_stk = [];
foreach ($data_barang as $value) {
    $temp_stk[]=$value['stok'];

    $max_j = max($temp_stk);
    $min_j = min($temp_stk);
}
// var_dump($max_j); exit;

// search pencarian
$penampung=[];
if(isset($_POST['cari']))
{
  $penampung=searching($_POST['search']);
}
if (!empty($penampung))
{
  $data_barang=$penampung;
}

// Search Action
if(isset($_POST['search']))
{

  $filter=$db->quote($_POST['search']);  

  $name=$_POST['search'];

  $search=$db->prepare("select * from data where nama_barang=? or kode_barang=? or harga=? or stok=?");

  $search->bindValue(1,$name,PDO::PARAM_STR);
  $search->bindValue(2,$name,pdo::PARAM_STR);
  $search->bindValue(3,$name,pdo::PARAM_INT);
  $search->bindValue(4,$name,pdo::PARAM_INT);

  $search->execute();

  $tampil_data=$search->fetchAll(); 

  $row = $search->rowCount();
}else{
  $data = $db->query("select * from data");

  $tampil_data = $data->fetchAll();
}
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/solid.css" integrity="sha384-yo370P8tRI3EbMVcDU+ziwsS/s62yNv3tgdMqDSsRSILohhnOrDNl142Df8wuHA+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/brands.css" integrity="sha384-/feuykTegPRR7MxelAQ+2VUMibQwKyO6okSsWiblZAJhUSTF9QAVR0QLk6YwNURa" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/fontawesome.css" integrity="sha384-ijEtygNrZDKunAWYDdV3wAZWvTHSrGhdUfImfngIba35nhQ03lSNgfTJAKaGFjk2" crossorigin="anonymous">
    <link rel="stylesheet" href="css/dashboard.css">
    <title>Dashboard Project Assessement</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <!-- Navbar -->
                <nav id="coba" class="navbar navbar-light bg-dark" style="z-index: 1;">
                    <h3 class="text-light pt-1">Street Clothing Official Store</h3>
                    <form class="form-inline my-2 my-lg-0" action="dashboard.php" method="POST">
                        <input name="search" class="form-control mr-sm-2" type="text" name="search" placeholder="Cari Data" autocomplete="off" aria-label="Search">
                        <button name="cari" class="btn btn-primary my-2 my-sm-0" type="submit">Cari</button>
                    </form>
                </nav>
            </div>
        </div>
    </div>
    
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <!-- Sidebar -->
                <div class="sidebar bg-dark col-12 col-sm-12 col-md-12 col-lg-12 text-center border">
                    <img class="mt-4" src="image/2.png" width="150" height="100">
                    <h3 id="color" style="font-family: Calibry" class="mt-1">Street Clothing</h3>
                    <hr class="bg-light mt-4">
                    <h3 class="mt-3"><a class="btn-outline-secondary" href="index.php"><i class="fas fa-home"></i> Home</a></h3><hr class="bg-light">
                    <h3 class="mt-3"><a class="btn-outline-dark" href="data.php"><i class="fas fa-table"></i> Data</a></h3><hr class="bg-light">
                    <p id="seting" class="text-center">Munif Soleh | &copy;2020</p>
                </div>
                
                <!-- Content -->
                <div class="content">
                    <div class="row" style="z-index: -1;">

                        <div class="col-sm-12">
                            <!-- Navbar -->
                            <nav class="navbar navbar-light">
                                <h3 class="text-light">Street Clothing Dashboard</h3>
                                <span class="jam"></span>
                            </nav>
                        </div>

                        <!-- Cards -->
                        <div class="col-sm-3 mb-3">
                            <div class="card text-light border">
                            <div class="card-body">
                                <i id="font" class="fas fa-comments-dollar"></i>
                                <h5 class="card-title">Harga Termahal</h5>
                                <p class="card-text">Rp.<?php echo $max; ?></p>
                            </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="card text-light border">
                            <div class="card-body">
                                <i id="font" class="fas fa-comment-dollar"></i>
                                <h5 class="card-title">Harga Termurah</h5>
                                <p class="card-text">Rp.<?php echo $min; ?></p>
                            </div>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="card text-light border">
                            <div class="card-body">
                                <i id="font" class="fas fa-arrow-circle-up"></i>
                                <h5 class="card-title">Stok Terbanyak</h5>
                                <p class="card-text"><?php echo $max_j; ?></p>
                            </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="card text-light border">
                            <div class="card-body">
                                <i id="font" class="fas fa-arrow-circle-down"></i>
                                <h5 class="card-title">Stok Tersedikit</h5>
                                <p class="card-text"><?php echo $min_j; ?></p>
                            </div>
                            </div>
                        </div>

                        <!-- Table -->
                        <div id="table" class="col col-12 col-sm-12 col-md-12 col-lg-12">
                            <!----- Allert Massage ----->
                            <?php if(isset($row)):?>
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                <p class="lead "><?php echo $row;?> Data Ditemukan !</p>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <?php endif; ?>
                            <!-- Table content -->
                            <table class="table table-striped">
                                <thead class="bg-dark text-light">
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Nama Barang</th>
                                        <th scope="col">Kode Barang</th>
                                        <th scope="col">Harga</th>
                                        <th scope="col">Stok</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $n=1 ; ?>
                                    <?php foreach ($data_barang as $key) : ?>
                                    <tr class="text-light">
                                        <td><?php echo $n; ?></td>
                                        <td><?php echo $key["nama_barang"]; ?></td>
                                        <td><?php echo $key["kode_barang"]; ?></td>
                                        <td><?php echo $key["harga"]; ?></td>
                                        <td><?php echo $key["stok"]; ?></td>
                                    </tr>
                                    <?php $n++ ; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function jam() {
        var time = new Date(),
            hours = time.getHours(),
            minutes = time.getMinutes(),
            seconds = time.getSeconds();
        document.querySelectorAll('.jam')[0].innerHTML = harold(hours) + ":" + harold(minutes) + ":" + harold(seconds);
        
        function harold(standIn) {
            if (standIn < 10) {
            standIn = '0' + standIn
            }
            return standIn;
            }
        }
        setInterval(jam, 1000);
    </script>


    <!-- Optional JavaScript; choose one of the two! -->
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    
</body>
</html>

